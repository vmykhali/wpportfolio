<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpmyportfolio' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         's;7yzmHn5jniANJx(6R6bCbn;=x*}J@|KqYwogic+|#*234xQy_GlXpiE}|x`5;D' );
define( 'SECURE_AUTH_KEY',  '6ag.qv~-:%mT/|KZV4:2LxQcb^y+JBECdrzz<WY7u^.qpo.Nl30%:Q_D}W<GC}48' );
define( 'LOGGED_IN_KEY',    'VNq8:QF S`cD;[l??nHltr6jAg=}a/L#XAwJQQ^$3A.I=w|@($in)A0vv5Y)oDU#' );
define( 'NONCE_KEY',        'E5uO=jRM&>NY-`7o)In26S*}W6$>Z=!m;`DJU%VfcyUk6%;o3M*BAnN<BG<w$4r:' );
define( 'AUTH_SALT',        'O+l0S6} Pj!:f~7J=v a#NsHW@1tCF_LHPT!k[nC=#KzsN2V82,V:a_%l^FU~B=K' );
define( 'SECURE_AUTH_SALT', 'J%Z>B kxc]x7sN|p+^WrF2b8d_{dVgWgg=9Y}&nd0|]//N/i`(2J)N.6l}76`]os' );
define( 'LOGGED_IN_SALT',   'Brjw$>5U&(0!#gJ x`z?@B_?b`-I[Y3g|8=m</SuWdpM_5g<cIz`$.|f=b.)?H2C' );
define( 'NONCE_SALT',       'xBah?AWJ/o^>+-nm(oxi7,$#`x:hk qTt <K?l(/r#D9%3Aou8*n`i*x9TsRmozM' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
