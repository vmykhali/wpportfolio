<footer class="third-bg">
    <div class="container">
        <div class="footer-nav">
            <?php
            wp_nav_menu(array(
                    'theme_location' => 'footermenu',
                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'menu_class' => 'nav navbar-nav footer-nav',
                    'menu_id' => '',
                    'depth' => 1
                )
            );
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer_top">
                    <h4> <?php echo get_theme_mod('header_social')?></h4>
                    <ul>
                        <?php if (get_theme_mod('facebook_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('facebook_social')?>"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                        <?php if (get_theme_mod('twitter_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('twitter_social')?>"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                        <?php if (get_theme_mod('linkedin_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('linkedin_social')?>"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                        <?php if (get_theme_mod('google_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('google_social')?>"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                        <?php if (get_theme_mod('youtube_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('youtube_social')?>"> <i class="fa fa-youtube-square" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                        <?php if (get_theme_mod('instagram_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('instagram_social')?>"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                        <?php endif?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>

<?php wp_footer();?>
</html>
