<article id="<?php the_ID();?>" <?php post_class('blog_post'); ?> >
    <h4> <?php the_title();?></h4>

    <div class="blog_category">
        <ul>
            <li> <?php the_category(', ');?></li>
        </ul>
    </div>

    <div class="blog_text">
        <ul>
            <li> | </li>
            <li>Post By : <?php the_author_posts_link() ?></li>
            <li> | </li>
            <li>  On : <?php the_time('j F Y');?> </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="footer_top nav_post_social">
                <h4> <?php echo get_theme_mod('header_social')?></h4>
                <ul>
                    <?php if (get_theme_mod('facebook_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('facebook_social')?>"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                    <?php if (get_theme_mod('twitter_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('twitter_social')?>"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                    <?php if (get_theme_mod('linkedin_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('linkedin_social')?>"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                    <?php if (get_theme_mod('google_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('google_social')?>"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                    <?php if (get_theme_mod('youtube_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('youtube_social')?>"> <i class="fa fa-youtube-square" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                    <?php if (get_theme_mod('instagram_social') != '') : ?>
                        <li> <a href="<?php echo get_theme_mod('instagram_social')?>"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                    <?php endif?>
                </ul>
            </div>
        </div>
    </div>
    <div class="blog_post_img">
         <?php the_post_thumbnail();?>
    </div>

    <?php the_content();?>
</article>