<?php get_header();?>
<?php get_template_part('template-parts/banner')?>
    <section class="post_blog_bg primary-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-8">
                        <?php the_post();?>
                        <?php get_template_part('template-parts/content', 'page');?>
                    </div>
                    <?php get_sidebar();?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();?>