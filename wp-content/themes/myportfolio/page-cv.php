<?php get_header();?>
<section class="cv-bg secondary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top_banner">
                    <div class="category_m">
                        <h1> Mykhalytskyi Vladyslav</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="cv-content">
    <div class="container cv-content-container">

        <div class="portfolio-photo"></div>
        <div class="professional-skills">
            <h4 class="professional-skills__h4">P E R S O N A L G O A L</h4>
            <p class="professional-skills__p">
            Develop in the direction of
            programming. I do self-study and
            have a great desire to move from
            theory to practice. I want to
            actively participate in the
            development of the company and
            create products that will
            contribute to the development of
            my career.
            </p>
            <h4 class="professional-skills__h4">E D U C A T I O N</h4>
            <ul class="professional-skills__ul">
                <li class="professional-skills__li"><p>
                        NTUU "KPI them. Igor
                        Sikorsky"
                        Faculty of Electrical Engineering
                        and Automation<br>
                        2014-2019
                    </p></li>
                <li class="professional-skills__li"><p>
                        C #, .NET and MS SQL self
                        study (base knowledge)<br>
                        April 2018 - present time
                    </p></li>
                <li class="professional-skills__li"><p>
                        JavaScript, PHP (base knowledge)<br>
                        January 2020 - present time
                    </p></li>

            </ul>
            <h4 class="professional-skills__h4">LET'S WORK TOGETHER!</h4>
            <p><strong>phone</strong>: +380997180141<br>
                <strong>email</strong>: icemicher@gmail.com</p>
        </div>

        <div class="personal-info">
            <h4 class="personal-info__h4">CAREER HISTORY</h4>
            <ul>
                <li>
                    <h5>Graphic Assistant</h5>
                    <p>MAY - JULY 2018</p>
                    <p class="personal-info__p">
                        <span>Mindy Supports</span>, Kyiv (Business Services)<br>
                        Work in Photoshop.
                    </p>
                </li>
                <li>
                    <h5>Database programmer</h5>
                    <p>JULY - AUGUST 2018</p>
                    <p class="personal-info__p">
                        <span>BPI Ukraine</span>, Kyiv (CRM ERP systems)<br>
                        Support and customization of business management programs
                    </p>
                </li>
                <li>
                    <h5>Internet store manager</h5>
                    <p>DECEMBER 2018 - JUNE 2019</p>
                    <p class="personal-info__p">
                        <span>Amazon</span>, Kyiv (<span>trading platform</span>)<br>
                        Shop administration and customer communication
                    </p>
                </li>
                <li>
                    <h5>Content manager</h5>
                    <p>AUGUST 2019 - DECEMBER 2019</p>
                    <p class="personal-info__p">
                        <span>Lightfield production</span>, Kyiv<br>
                        Content administration.
                    </p>
                </li>
            </ul>
        </div>
        <aside class="col-md-6 cv-sidebar">
            <?php if(is_active_sidebar('sidebar-2')) : ?>
                <div class="side_blog_bg">
                    <?php dynamic_sidebar('sidebar-2')?>
                </div>
            <?php endif ?>
        </aside>
    </div>
</div>
<?php get_footer();?>
