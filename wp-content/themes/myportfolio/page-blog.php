<?php get_header();?>

<?php get_template_part('template-parts/banner')?>

<section class="post_blog_bg primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-8">
                    <?php
                    $doc = new DOMDocument;
                    $content = file_get_contents('https://coinspot.io/feed/');
                    $items = new SimpleXmlElement( $content );
                    foreach ( $items->channel->item as $key => $item ) {
                    echo '<div class="news__item">';
                        echo "<h4>".$item->title."</h4>";
                        echo '<img src="'.$item->enclosure['url'].'">';
                        echo '<p>'.$item->description.'</p>';
                        echo '<br><br><a href="'.$item->link. '">View more</a><br>';
                        echo '<br></div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>
