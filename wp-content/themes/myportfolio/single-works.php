<?php get_header();?>

<?php get_template_part('template-parts/banner')?>



    <section class="post_blog_bg primary-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-8">
                        <?php the_post();?>
                        <?php get_template_part('template-parts/content', 'single');?>
                    </div>
                    <aside class="col-md-4">
                        <?php if(is_active_sidebar('sidebar-2')) : ?>
                            <div class="side_blog_bg">
                                <?php dynamic_sidebar('sidebar-2')?>
                            </div>
                        <?php endif ?>
                    </aside>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();?>