<?php

function myportfolio_setup() {

//    load_theme_textdomain('myportfolio');
    add_theme_support('title-tag'); //добавляет тайтл

    add_theme_support('custom-logo', array(
        'height' => 50,
        'width' => 98,
        'flex-height' => true));

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(730, 446);

    add_theme_support('html5', array(
        'search_form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    ));

    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'gallery',
    ));


    register_nav_menu('footermenu', 'Footer menu');
    register_nav_menu('primary', 'Primary menu');

}

add_action('after_setup_theme', 'myportfolio_setup');


// правильный способ подключить стили и скрипты
add_action( 'wp_enqueue_scripts', 'myportfolio_scripts' );
// add_action('wp_print_styles', 'theme_name_scripts'); // можно использовать этот хук он более поздний
function myportfolio_scripts() {
    wp_enqueue_style( 'my_style', get_stylesheet_uri() );
    wp_enqueue_style( 'animate', get_template_directory_uri() . "/css/animate.min.css" );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . "/css/bootstrap.css" );
    wp_enqueue_style( 'awesome', get_template_directory_uri() . "/css/font-awesome.min.css" );


    wp_enqueue_script( 'jquery', get_template_directory_uri() . 'js/jquery.min.js');
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . 'js/bootstrap.min.js');
    wp_enqueue_script( 'animate', get_template_directory_uri() . 'js/css3-animate-it.js');
    wp_enqueue_script( 'easing', get_template_directory_uri() . 'js/jquery.easing.min.js');
    wp_enqueue_script( 'agency', get_template_directory_uri() . 'js/agency.js');
    wp_enqueue_script( 'interface', get_template_directory_uri() . 'js/interface.js');
}


add_filter('excerpt_more', function($more) {
    return '...';
});
/*
 * PAGINATION!!!
 *
 * */
function myportfolio_pagination( $args = array() ) {

    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => __( 'Previous', 'text-domain' ),
        'next_string'     => __( 'Next', 'text-domain' ),
        'before_output'   => '<div class="nex-page"><ul class="page-numbers">',
        'after_output'    => '</ul></div>'
    );

    $args = wp_parse_args(
        $args,
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );

    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );

    if ( $count <= 1 )
        return FALSE;

    if ( !$page )
        $page = 1;

    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }

    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );

//    $firstpage = esc_attr( get_pagenum_link(1) );
//    if ( $firstpage && (1 != $page) )
//        $echo .= '<li class="previous"><a href="' . $firstpage . ' "class="page-numbers">' . __( 'First', 'text-domain' ) . '</a></li>';

    if ( $previous && (1 != $page) )
        $echo .= '<li><a href="' . $previous . '"class="page-numbers" title="' . __( 'previous', 'text-domain') . '">' . $args['previous_string'] . '</a></li>';

    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="active"><span class="page-numbers current">' . str_pad( (int)$i, 1, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li><a class="page-numbers" href="%s">%2d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }

    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li><a class="page-numbers" href="' . $next . '" title="' . __( 'next', 'text-domain') . '">' . $args['next_string'] . '</a></li>';

//    $lastpage = esc_attr( get_pagenum_link($count) );
//    if ( $lastpage ) {
//        $echo .= '<li class="next"><a class="page-numbers" href="' . $lastpage . '">' . __( 'Last', 'text-domain' ) . '</a></li>';
//    }

    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}

function myportfolio_customize_register( $wp_customize ) {
    $wp_customize->add_setting('header_social', array(
        'default' => __('Share Your Text', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('facebook_social', array(
        'default' => __('Url facebook', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('twitter_social', array(
        'default' => __('Url twitter', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('linkedin_social', array(
        'default' => __('Url linkedin', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('google_social', array(
        'default' => __('Url google', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('youtube_social', array(
        'default' => __('Url youtube', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('instagram_social', array(
        'default' => __('Url instagram', 'myportfolio'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('social_section', array(
        'title' => __('Social settings', 'myportfolio'),
        'priority' => 30,
    ));

    $wp_customize->add_control('header_social', array(
        'label' => __('Social header in footer settings', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'header_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('facebook_social', array(
        'label' => __('Facebook profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'facebook_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('twitter_social', array(
        'label' => __('Twitter profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'twitter_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('linkedin_social', array(
        'label' => __('Linkedin profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'linkedin_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('google_social', array(
        'label' => __('Google profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'google_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('youtube_social', array(
        'label' => __('Youtube profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'youtube_social',
        'type' => 'text',
    ));

    $wp_customize->add_control('instagram_social', array(
        'label' => __('Instagram profile url', 'myportfolio'),
        'section' => 'social_section',
        'settings' => 'instagram_social',
        'type' => 'text',
    ));
}
add_action( 'customize_register', 'myportfolio_customize_register' );

function myportfolio_widgets_init() {
    register_sidebar( array(
        'name' => __('Main Sidebar', 'myportfolio'),
        'id' => 'sidebar-1',
        'description' => __('Выводиться как боковая панель только на главной странице сайта.', 'myportfolio'),
        'before_widget' => '<div id="%1$s" class="sidebar_wrap %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="side_bar_heading"><h6>',
        'after_title' => '</h6></div>',
    ) );

    register_sidebar( array(
        'name' => __('Taxonomy Sidebar', 'myportfolio'),
        'id' => 'sidebar-2',
        'description' => __('Выводиться как боковая панель только на главной странице сайта.', 'myportfolio'),
        'before_widget' => '<div id="%1$s" class="sidebar_wrap %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="side_bar_heading"><h6>',
        'after_title' => '</h6></div>',
    ) );
}

add_action( 'widgets_init', 'myportfolio_widgets_init' );

class Walker_Categoryes_Myportfolio extends Walker_Category {
    /**
     * What the class handles.
     *
     * @since 2.1.0
     * @var string
     *
     * @see Walker::$tree_type
     */
    public $tree_type = 'category';

    /**
     * Database fields to use.
     *
     * @since 2.1.0
     * @var array
     *
     * @see Walker::$db_fields
     * @todo Decouple this
     */
    public $db_fields = array(
        'parent' => 'parent',
        'id'     => 'term_id',
    );

    /**
     * Starts the list before the elements are added.
     *
     * @since 2.1.0
     *
     * @see Walker::start_lvl()
     *
     * @param string $output Used to append additional content. Passed by reference.
     * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
     * @param array  $args   Optional. An array of arguments. Will only append content if style argument
     *                       value is 'list'. See wp_list_categories(). Default empty array.
     */
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        parent::start_lvl($output, $depth, $args );
    }

    /**
     * Ends the list of after the elements are added.
     *
     * @since 2.1.0
     *
     * @see Walker::end_lvl()
     *
     * @param string $output Used to append additional content. Passed by reference.
     * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
     * @param array  $args   Optional. An array of arguments. Will only append content if style argument
     *                       value is 'list'. See wp_list_categories(). Default empty array.
     */
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        parent::end_lvl($output, $depth, $args);
    }

    /**
     * Starts the element output.
     *
     * @since 2.1.0
     *
     * @see Walker::start_el()
     *
     * @param string $output   Used to append additional content (passed by reference).
     * @param object $category Category data object.
     * @param int    $depth    Optional. Depth of category in reference to parents. Default 0.
     * @param array  $args     Optional. An array of arguments. See wp_list_categories(). Default empty array.
     * @param int    $id       Optional. ID of the current category. Default 0.
     */
    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        /** This filter is documented in wp-includes/category-template.php */
        $cat_name = apply_filters( 'list_cats', esc_attr( $category->name ), $category );

        // Don't generate an element if the category name is empty.
        if ( '' === $cat_name ) {
            return;
        }

        $atts         = array();
        $atts['href'] = get_term_link( $category );

        if ( $args['use_desc_for_title'] && ! empty( $category->description ) ) {
            /**
             * Filters the category description for display.
             *
             * @since 1.2.0
             *
             * @param string $description Category description.
             * @param object $category    Category object.
             */
            $atts['title'] = strip_tags( apply_filters( 'category_description', $category->description, $category ) );
        }

        /**
         * Filters the HTML attributes applied to a category list item's anchor element.
         *
         * @since 5.2.0
         *
         * @param array   $atts {
         *     The HTML attributes applied to the list item's `<a>` element, empty strings are ignored.
         *
         *     @type string $href  The href attribute.
         *     @type string $title The title attribute.
         * }
         * @param WP_Term $category Term data object.
         * @param int     $depth    Depth of category, used for padding.
         * @param array   $args     An array of arguments.
         * @param int     $id       ID of the current category.
         */
        $atts = apply_filters( 'category_list_link_attributes', $atts, $category, $depth, $args, $id );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
                $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $link = sprintf(
            '<a%s><i class="fa fa-folder-open-o" aria-hidden="true"></i>%s</a>',
            $attributes,
            $cat_name
        );

        if ( ! empty( $args['feed_image'] ) || ! empty( $args['feed'] ) ) {
            $link .= ' ';

            if ( empty( $args['feed_image'] ) ) {
                $link .= '(';
            }

            $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $args['feed_type'] ) ) . '"';

            if ( empty( $args['feed'] ) ) {
                /* translators: %s: Category name. */
                $alt = ' alt="' . sprintf( __( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
            } else {
                $alt   = ' alt="' . $args['feed'] . '"';
                $name  = $args['feed'];
                $link .= empty( $args['title'] ) ? '' : $args['title'];
            }

            $link .= '><i class="fa fa-folder-open-o" aria-hidden="true"></i>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= $name;
            } else {
                $link .= "<img src='" . esc_url( $args['feed_image'] ) . "'$alt" . ' />';
            }
            $link .= '</a>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= ')';
            }
        }

//        if ( ! empty( $args['show_count'] ) ) {
//            $link .= ' (' . number_format_i18n( $category->count ) . ')';
//        }
        if ( ! empty( $args['show_count'] ) ) {
            $link .= '<span>' . number_format_i18n( $category->count ) . '</span>';
        }
        if ( 'list' == $args['style'] ) {
            $output     .= "\t<li";
            $css_classes = array(
                'cat-item',
                'cat-item-' . $category->term_id,
            );

            if ( ! empty( $args['current_category'] ) ) {
                // 'current_category' can be an array, so we use `get_terms()`.
                $_current_terms = get_terms(
                    array(
                        'taxonomy'   => $category->taxonomy,
                        'include'    => $args['current_category'],
                        'hide_empty' => false,
                    )
                );

                foreach ( $_current_terms as $_current_term ) {
                    if ( $category->term_id == $_current_term->term_id ) {
                        $css_classes[] = 'current-cat';
                        $link          = str_replace( '<a', '<a aria-current="page"', $link );
                    } elseif ( $category->term_id == $_current_term->parent ) {
                        $css_classes[] = 'current-cat-parent';
                    }
                    while ( $_current_term->parent ) {
                        if ( $category->term_id == $_current_term->parent ) {
                            $css_classes[] = 'current-cat-ancestor';
                            break;
                        }
                        $_current_term = get_term( $_current_term->parent, $category->taxonomy );
                    }
                }
            }

            /**
             * Filters the list of CSS classes to include with each category in the list.
             *
             * @since 4.2.0
             *
             * @see wp_list_categories()
             *
             * @param array  $css_classes An array of CSS classes to be applied to each list item.
             * @param object $category    Category data object.
             * @param int    $depth       Depth of page, used for padding.
             * @param array  $args        An array of wp_list_categories() arguments.
             */
            $css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );
            $css_classes = $css_classes ? ' class="' . esc_attr( $css_classes ) . '"' : '';

            $output .= $css_classes;
            $output .= ">$link\n";
        } elseif ( isset( $args['separator'] ) ) {
            $output .= "\t$link" . $args['separator'] . "\n";
        } else {
            $output .= "\t$link<br />\n";
        }
    }

    /**
     * Ends the element output, if needed.
     *
     * @since 2.1.0
     *
     * @see Walker::end_el()
     *
     * @param string $output Used to append additional content (passed by reference).
     * @param object $page   Not used.
     * @param int    $depth  Optional. Depth of category. Not used.
     * @param array  $args   Optional. An array of arguments. Only uses 'list' for whether should append
     *                       to output. See wp_list_categories(). Default empty array.
     */
    public function end_el( &$output, $page, $depth = 0, $args = array() ) {
        parent::end_el($output, $page, $depth, $args);
    }
}
function myportfolio_widget_categories ($args) {
    $walker = new Walker_Categoryes_Myportfolio();
    $args = array_merge($args, array('walker' => $walker));

    return $args;
}

add_filter('widget_categories_args', 'myportfolio_widget_categories');

function myportfolio_tag_cloud($args) {
    $args['format'] = 'list';
    $args['smallest'] = '14';
    $args['largest'] = '22';
    $args['unit'] = 'px';
    return $args;
}

add_filter('widget_tag_cloud_args','myportfolio_tag_cloud');

add_action( 'init', 'myportfolio_register_post_type_init' ); // Использовать функцию только внутри хука init

function myportfolio_register_post_type_init() {
    $labels = array(
        'name' => 'Development',
        'singular_name' => 'Developments', // админ панель Добавить->Функцию
        'add_new' => 'Add New',
        'add_new_item' => 'Add new development', // заголовок тега <title>
        'edit_item' => 'Edit development',
        'new_item' => 'New development',
        'all_items' => 'All developments',
        'view_item' => 'View developments on site',
        'search_items' => 'Search all developments',
        'not_found' =>  'Developments not found.',
        'not_found_in_trash' => 'No development in the basket.',
        'menu_name' => 'Works' // ссылка в меню в админке
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_menu' => true,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'show_ui' => true, // показывать интерфейс в админке
        'has_archive' => true,
        'menu_icon' => get_stylesheet_directory_uri() .'/images/no.png', // иконка в меню
        'menu_position' => 4, // порядок в меню
        'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail')
    );
    register_post_type('works', $args);
}
// функция, создающая таксономию "Разделы" для постов типа "Товары"

function create_myportfolio_taxonomies(){

    // определяем заголовки для 'Works'
    $labels = array(
        'name' => _x( 'Technologies', 'taxonomy general name' ),
        'singular_name' => _x( 'Technology', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Technologies' ),
        'popular_items' => __( 'Popular Technologies' ),
        'all_items' => __( 'All Technologies' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Technology' ),
        'update_item' => __( 'Refresh Technology' ),
        'add_new_item' => __( 'Add new Technology' ),
        'new_item_name' => __( 'Name of new Technology' ),
        'separate_items_with_commas' => __( 'Separate writers with commas' ),
        'add_or_remove_items' => __( 'Add or delete Technology' ),
        'choose_from_most_used' => __( 'Choose from the most used writers' ),
        'menu_name' => __( 'Technologies' ),

    );
// Добавляем древовидную таксономию 'Разделы' (как рубрики), чтобы сделать НЕ девовидную (как метки) значение для 'hierarchical' => false,

    register_taxonomy('technologies', 'works',array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_admin_column' => true,
        'show_in_nav_menus ' => true,
        'show_tagcloud' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'technologies' ),
    ));
}

add_action( 'init', 'create_myportfolio_taxonomies', 0 );

